/**
 * *Lambda*
 *
 * Zadanie 1
 * 1.    Stwórz interfejs IntSequence zawierający dwie metody:
 * a.    hasNext() – zwraca boolean (defaultowo zwraca true)
 * b.    next() – zwraca int
 * 2.    Wszystkie poniższe klasy mają implementować IntSequence
 * 3.    Stwórz klasę PrimeSequence która z każdym wywołaniem metoda next(), będzie zwracała kolejne liczby pierwsze, mniejsze od 2000
 * a.    Jeżeli metoda next zwróci ostatnią liczbę pierwszą < 2000, metoda hasNext() powinna zwracać fałsz
 * 4.    Stwórz klasę SquareSequence która wraz z każdym wywołaniem metody next() zwraca kolejne kwadraty liczb dodatnich -> 1,4,9,16, …
 * 5.    Stwórz klasę DigitSequence która wraz z każdym wywołaniem metody next() zwraca kolejne cyfry liczby (od końca) przekazanej jako konstruktor
 * Np. DigitSequence(1236) -> 6, 3, 2, 1
 * 6.    W klasie Main, stwórz metodę statyczną average przyjmującą parametry
 * a.    Jedną z powyższych klas (chcemy aby ta jedna metoda miała możliwość załadowania jako argumentu jednej z powyższych klas – zastanów się jak napisać taką uniwersalną metodę)
 * b.    Int n
 * Jej zadaniem jest policzenie średniej z liczb dopóki:
 *     Ilość liczb branych pod uwagę jest mniejsza od n lub metoda hasNext() zwróci fałsz
 *
 * Np. dla DigitSequence(1236) i parametru n = 4, funkcja powinna zwracać 3
 * Dla DigitSequence(1236) i parametru n = 3, funkcja powinna zwracać 2
 * Dla DigitSequence(1236) i parametru n = 5, funkcja powinna zwracać 3
 * Dla PrimeSequence() i parametru n=4, funkcja powinna zwracać 7.5 - (1+4+9+16)/4
 *
 * Przetestuj wszystkie klasy, sprawdź czy Twoja funkcja jest oporna na błędy
 */


package LambdaZadanie1;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LambdaTest {

    private static PrimeSequence ps;
    private static List<Integer> primeSequenceList;

    @Test
    void firstUseNext(){
        //Given
        ps = new PrimeSequence();

        //When
        int result = ps.next();

        //Then
        assertEquals(2,result);

    }

    @Test
    void secondUseNext(){
        //Given
        ps = new PrimeSequence();

        //When
        int result = 0;
        int i;
        for(i = 0; i < 2; i++ ){
            primeSequenceList.add(result);
        }
        primeSequenceList.get(i);

        //Then
        assertEquals(3,result);

    }

    @Test
    void thirdUseNext(){
        //Given
        ps = new PrimeSequence();

        //When
        int result = 0;
        int i;
        for(i = 0; i < 3; i++ ){
            primeSequenceList.add(result);
        }
        primeSequenceList.get(i);

        //Then
        assertEquals(5,result);
    }

}