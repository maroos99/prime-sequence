package LambdaZadanie1;

public interface IntSequence {

    boolean hasNext();
    int next();

}
